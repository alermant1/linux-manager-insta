var exec = require('exec-then');

module.exports = function(username, password, next) {

  // si username != root on refuse
  // supprimer cette ligne pour accepter n'importe quel utilisateur
  // enregistré dans la machine
  if (username !== 'root') {
    return next(null, false);
  }

  // 1°) on récupère la liste des utilisateurs et mot de passe hashé dans /etc/shadow
  // 2°) on cherche l'user qui nous interesse
  // 3°) on split (par rapport à `:`) et on prend le 2eme index du résultat
  // => ce qui nous donne le mot de passe hashé
  exec(`cat /etc/shadow | grep ${username}: | awk -F: '{print $2}'`, function(
    std, deferred) {
    return std.stdout;
  }).then(function(retour) {
    let secret = retour.params.split('$')[2];
    // on créé un password avec la commande mkpasswd et on utilise le meme hash
    exec(`mkpasswd -m sha-512 -S ${secret} ${password}`, function(s, d) {
      // on vérifie si le mot de passe généré est le meme que dans /etc/shadow
      if (retour.params !== s.stdout) {
        next(null, false);
      } else {
        next(null, true);
      }
    });
  });


}
