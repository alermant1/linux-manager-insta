var isRoot = require('is-root');
if (!isRoot()) {
  console.log("*********************************************************");
  console.log("*                                                       *");
  console.log("* Merci de lancer l'application entant que root ou sudo *");
  console.log("*                  ex:  `sudo npm start`                *");
  console.log("*                                                       *");
  console.log("*********************************************************");
  process.exit();
}

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var socket_io = require('socket.io');

var app = express();
var io = socket_io();
app.io = io;

var index = require('./routes/index');
var monitoring = require('./routes/monitoring');
var monitoringEvent = require('./event/monitoring');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
var exphbs = require('express-handlebars');
app.engine('handlebars', exphbs({
  extname: 'hbs',
  partialsDir: __dirname + '/views/partials/',
  layoutsDir: __dirname + '/views/layouts/'
}));
app.set('view engine', 'handlebars');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// authentification root require
var basicAuth = require('express-basic-auth');
app.use(basicAuth({
  authorizer: require('./auth.js'),
  authorizeAsync: true,
  // demande au navigateur
  challenge: true,
  // si pas authorisé
  unauthorizedResponse: function(req) {
    if (req.auth) {
      return 'Identifiants incorrects'
    } else {
      return 'Identifiants manquants'
    }
  }
}))

app.use('/', monitoring);
app.use('/monitoring', monitoring);
app.use('/gestion-utilisateurs', require('./routes/gestion-utilisateurs'));
app.use('/gestion-groupes', require('./routes/gestion-groupes'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

var sockets = {};

// quand l'utilisateur accède à une page
io.on('connection', function(socket) {
  sockets[socket.id] = socket;

  // quand l'utilisateur demande monitoring
  socket.on('monitoring', function(msg) {
    return monitoringEvent().then(res => {
      socket.emit('monitoring', {
        memoire: res[0].params,
        services: res[1].params,
        interfaces: res[2].params,
        processus: res[3].params,
        cpuUse: res[4].params
      });
    });
  });

  socket.on('add-groupe', function(string_groupe) {
    require('./event/gestion-utilisateurs/groupe/add-groupe')(
      string_groupe);
  });

  socket.on('del-groupe', function(string_groupe) {
    require('./event/gestion-utilisateurs/groupe/del-groupe')(
      string_groupe);
  });

  socket.on('list-groupe', function() {
    require('./event/gestion-utilisateurs/groupe/list-groupe')().then(
      res => {
        socket.emit('list-groupe', {
          list: res[0].params
        })
      });
  });

  socket.on('add-groupe-utilisateur', function(obj_groupe_utilisateur) {
    // {groupe: string_groupe, utilisateur: string_utilisateur}
    require(
      './event/gestion-utilisateurs/groupe/add-groupe-utilisateur')(
      obj_groupe_utilisateur);
  });

  socket.on('add-utilisateur', function(obj_groupe_utilisateur) {
    // {name: string_nom, password: string_password}
    require('./event/gestion-utilisateurs/utilisateur/add-utilisateur')
      (obj_groupe_utilisateur);
  });

  socket.on('del-groupe-utilisateur', function(obj_groupe_utilisateur) {
    // {groupe: string_groupe, utilisateur: string_utilisateur}
    require(
      './event/gestion-utilisateurs/groupe/del-groupe-utilisateur')(
      int_id_groupe);
  });

  socket.on('del-utilisateur', function(string_utilisateur) {
    require('./event/gestion-utilisateurs/utilisateur/del-utilisateur')
      (string_utilisateur);
  });

  socket.on('list-utilisateur', function() {
    require('./event/gestion-utilisateurs/utilisateur/list-utilisateur')
      ().then(res => {
        socket.emit('list-utilisateur', {
          list: res[0].params
        })
      });
  });

  socket.on('disconnect', function() {
    delete sockets[socket.id];
  });
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
