var exec = require('exec-then');

var memoire = function() {
  return exec(['cat', '/proc/meminfo'], function(std, deferred) {

    let arrayOutput = std.stdout.split('\n');
    let retour = {
      total: arrayOutput[0].replace(/\D/g, ''),
      libre: arrayOutput[1].replace(/\D/g, ''),
      buffers: arrayOutput[3].replace(/\D/g, ''),
      cached: arrayOutput[4].replace(/\D/g, ''),
      swapCached: arrayOutput[5].replace(/\D/g, ''),
      swapTotal: arrayOutput[14].replace(/\D/g, ''),
      swapFree: arrayOutput[15].replace(/\D/g, '')
    };

    return retour;
  });
};

var cpuUse = function() {
  return exec(`ps -eo %cpu | grep -v CPU | awk '{total = total+$1}END{print total}'`,
    function(std, deferred) {
      return std.stdout;
    });
};

var services = function() {
  return exec(['systemctl', 'list-units', '--type=service'], function(std,
    deferred) {
    let arrayOutput = std.stdout.split('\n');
    arrayOutput.shift();
    for (var i = 0; i < arrayOutput.length; i++) {
      let element = arrayOutput[i];
      arrayOutput[i] = element.substring(0, element.indexOf("loaded"));
    };
    let retour = {
      service: arrayOutput
    };

    return retour;
  });
};

var interfaces = function() {
  return exec(['cat', '/proc/net/dev'], function(std, deferred) {
    let arrayOutput = std.stdout.split('\n');
    arrayOutput.shift();
    arrayOutput.shift();
    for (var i = 0; i < arrayOutput.length; i++) {
      let element = arrayOutput[i];
      arrayOutput[i] = element.substring(0, element.indexOf(":"));
    };
    let retour = [];
    arrayOutput.forEach(function(element) {
      element = element.replace(/\s+/g, '');
      let data = {};
      if (element != "") {
        data.name = element;
        receivePacket(element).then(res => {
          data.receivePacket = res.params;
        });
        receiveByte(element).then(res => {
          data.receiveByte = res.params;
        });
        transmitByte(element).then(res => {
          data.transmitByte = res.params;
        });
        transmitPacket(element).then(res => {
          data.transmitPacket = res.params;
          retour.push(data);
        });

      }
    });
    return retour;
  });
};



var processus = function() {
  return exec(`ps aux | awk '{ print $1 ";" $2 ";" $3 ";" $4 ";" $11}'`,
    function(std, deferred) {
      let arrayOutput = std.stdout.split('\n');
      let retour = [];
      for (var i = 0; i < arrayOutput.length; i++) {
        let element = arrayOutput[i];
        element = element.split(';');
        let data = {};
        data.user = element[0];
        data.pid = element[1];
        data.cpu = element[2];
        data.mem = element[3];
        data.command = element[4];
        retour.push(data);
      };
      return retour;
    });
};


var receivePacket = function(interFace) {
  return exec(['cat', `/sys/class/net/${interFace}/statistics/rx_packets`],
    function(std, deferred) {
      return std.stdout.replace('\n', '');
    });
};

var receiveByte = function(interFace) {
  return exec(['cat', `/sys/class/net/${interFace}/statistics/rx_bytes`],
    function(std, deferred) {
      return std.stdout.replace('\n', '');
    });
};

var transmitByte = function(interFace) {
  return exec(['cat', `/sys/class/net/${interFace}/statistics/tx_bytes`],
    function(std, deferred) {
      return std.stdout.replace('\n', '');
    });
};

var transmitPacket = function(interFace) {
  return exec(['cat', `/sys/class/net/${interFace}/statistics/tx_bytes`],
    function(std, deferred) {
      return std.stdout.replace('\n', '');
    });
};

module.exports = function() {
  let cmds = [memoire(), services(), interfaces(), processus(), cpuUse()];
  return Promise.all(cmds);
}
