var exec = require('exec-then');

var listGroupe = function() {
  return exec("cat /etc/group | awk -F: '{print $ 1}'", function(std, deferred) {
    return std.stdout.split('\n');
  });
};

module.exports = function() {
  let cmds = [listGroupe()];
  return Promise.all(cmds);
}

