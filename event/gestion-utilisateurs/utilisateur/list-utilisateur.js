var exec = require('exec-then');

var listUtilisateur = function() {
  return exec("cat /etc/passwd | awk -F: '{print $ 1}'", function(std, deferred) {
    return std.stdout.split('\n');
  });
};

module.exports = function() {
  let cmds = [listUtilisateur()];
  return Promise.all(cmds);
}

