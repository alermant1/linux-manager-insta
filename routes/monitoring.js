var express = require('express');
var router = express.Router();
var exec = require('exec-then');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('monitoring', {
    title: 'Express'
  });
});

module.exports = router;
